#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
int main(void)
{
	wiringPiSetup();
	pinMode(0, INPUT);
	pinMode(2, OUTPUT);
	
	for(;;)
	{
		if(digitalRead(0) == HIGH)
		{
			printf("no smoke!\n");
		}
		else 
		{
			system("/root/led");  //buzzer alarm
			system("/usr/bin/python3 /root/bin/exam6.py");  //SMS_alarm
		}
	}
	return 0;
}