# Q&A
### 1.安装第三方库opencv、pillow、numpy
用pip3安装好opencv后，import cv2时遇到问题
  ```bash
  ImportError: libGL.so.1: cannot open shared object file: No such file or directory
  ```

解决方法：官方软件源有一些mesa-libGL开头的包
  ```bash
  dnf install mesa-libGL
  ```

### 2.问题：调用cv2.imshow()函数时，会出现如下情况：

![Image description](https://images.gitee.com/uploads/images/2021/0901/110904_5cf1ee94_7373327.png "屏幕截图.png")

解决方法：cv2.imshow()函数是用于将摄像头画面和识别结果展示出来，需要加载一些QT组件，我下载的openeuler 21.03树莓派版本没有适配，因此无法使用该函数。这里我转换了下思路，既然无法获取适配画面，那只需要获取识别的结果就好了，树莓派接收到摄像头传回来的视频流，给出识别结果。

### 3.问题：本来想用socket连接把摄像头画面传输到PC上，在同一个局域网下，但是因为用到picamera库出问题。

![Image description](https://images.gitee.com/uploads/images/2021/0901/110942_80735804_7373327.png "屏幕截图.png")

不能确定该libbcm_host.so是属于哪个包

![Image description](https://images.gitee.com/uploads/images/2021/0901/110956_5d9a4310_7373327.png "屏幕截图.png")

![Image description](https://images.gitee.com/uploads/images/2021/0901/111001_85569237_7373327.png "屏幕截图.png")

根据树莓派固件库仓库下的目录，该libbcm_host.so文件是属于/opt/vc目录下的，但是在openeuler 21.03树莓派版本下没有该目录，/opt为空目录。尝试自行下载libbcm_host.so文件，放在sys.path和picamera库所在目录下，仍出现相同问题。

解决方法：[issue链接](https://gitee.com/openeuler/raspberrypi/issues/I4379H#note_6204808)

### 4.带图形桌面openeuler树莓派版本的内存占用非常大，正常开机后占用超过一半内存，因此不太适合较低配置的树莓派3B。
### 5.树莓派开机后，用ssh初次连接，此时的内存开支较少，判断应该是桌面程序没有开机自启动，系统版本是openeuler 21.03内测版，带DDE桌面、中文输入法。由于事先开启vnc，此时连接到的画面黑屏，开启步骤：OpenEuler通过VNC登录图形参考

### 6.制作数据集时，需要注意，有两个因素会影响到人脸识别效果：
- 人脸的光线，即是人脸清晰度。
- 单个人脸对应的数据集数量。
