# 环境配置，安装第三方库opencv、pillow、numpy
- ```bash
  pip install numpy
  ```

- ```bash
  pip install opencv-contrib-python
  ```
  
- ```bash
  pip install pillow
  ```  

# 1.测试摄像头，保存照片和视频，push 到项目仓库下面。
保存照片代码流程图：

![Image description](https://images.gitee.com/uploads/images/2021/0831/224947_5a450e3b_7373327.png "照片保存流程图.png")
# 2.人脸检测，人脸识别的第一步是要先找到画面中的人脸位置。opencv 有多种预训练 haar 级联分类器，可用于检测人脸、眼睛、笑容等，在 github代码仓下载后可用，这里我将其 fork 到了我的 gitee 仓库，地址：[分类器](https://gitee.com/daaaaaaaaaaa/opencv/tree/master/data/haarcascades)
代码流程图如下

![Image description](https://images.gitee.com/uploads/images/2021/0831/225112_e9048d40_7373327.png "人脸检测流程图.png")
> 注：实际上因为无法开启 vnc 以及缺少 QT 组件，人脸检测的结果并不直观，可以用 PC 或者用其他树莓派系统验证。

# 3.收集数据，创建数据集，该数据集储存每张人脸的 ID 和一组该人脸的灰度图。
先创建一个项目目录
  ```bash
  mkdir ./opencv
  ```
在项目目录创建子目录，用于放置数据集
  ```bash
  mkdir ./dataset
  ```
在人脸检测的基础上，输入人脸 ID，将每帧人脸转为灰度图，输出 30 张图片到子目录 dataset。
测试用例中，收集了两个人脸，各自 30 张灰度照片，ID 序号为 0 对应的名称为CMX

![Image description](https://images.gitee.com/uploads/images/2021/0831/225520_782acc3f_7373327.png "CMX.PNG")

ID 序号为 1 对应的名称为 AOBAMA

![Image description](https://images.gitee.com/uploads/images/2021/0831/225537_bf31b116_7373327.jpeg "奥巴马.jpg")

# 4.训练模型。
先创建一个子目录，用于保存训练得到的文件。
  ```bash
  mkdir ./trainer
  ```
流程图如下：

![Image description](https://images.gitee.com/uploads/images/2021/0831/225646_0433addc_7373327.png "训练模型流程图.png")

# 5.用训练好的人脸识别器识别人脸。
代码流程图如下：

![Image description](https://images.gitee.com/uploads/images/2021/0831/225741_db88ef97_7373327.png "人脸识别过程.png")

# 识别结果

![Image description](https://images.gitee.com/uploads/images/2021/0831/225909_5d52c6ed_7373327.png "识别结果.PNG")



